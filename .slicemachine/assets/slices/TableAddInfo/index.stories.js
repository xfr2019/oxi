import MyComponent from '../../../../slices/TableAddInfo';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/TableAddInfo',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'table_add_info',
        items: [
          {
            title: [
              {
                type: 'paragraph',
                text:
                  'Est labore aliqua ea nostrud est duis pariatur qui est consequat. Officia proident id amet id commodo aliqua quis elit sunt incididunt mollit deserunt aute do amet. Ex exercitation veniam dolore enim mollit ex reprehenderit amet velit aliqua laboris deserunt labore laborum reprehenderit.',
                spans: [],
              },
            ],
            undertitle: [
              {
                type: 'paragraph',
                text:
                  'Anim consectetur sunt anim mollit amet id velit minim sit quis sit magna consectetur tempor. Culpa veniam cupidatat culpa ullamco dolor laboris commodo duis velit elit do non minim in pariatur.',
                spans: [],
              },
            ],
            link: [
              {
                type: 'paragraph',
                text: 'Laborum ad nostrud aliqua commodo ut dolore est culpa non nulla.',
                spans: [],
              },
            ],
          },
          {
            title: [
              {
                type: 'paragraph',
                text:
                  'Quis elit reprehenderit et eiusmod officia consequat occaecat. Deserunt ad ullamco laboris sit cupidatat magna et in qui nulla adipisicing. Deserunt sit incididunt elit proident est qui.',
                spans: [],
              },
            ],
            undertitle: [
              {
                type: 'paragraph',
                text:
                  'Sit officia irure non pariatur. Ad sit sint commodo nisi voluptate ut. In culpa anim exercitation consequat anim Lorem ipsum nisi incididunt non deserunt cupidatat consequat reprehenderit.',
                spans: [],
              },
            ],
            link: [
              { type: 'paragraph', text: 'Fugiat commodo duis tempor esse excepteur.', spans: [] },
            ],
          },
        ],
        primary: {},
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
