import MyComponent from '../../../../slices/CardBlock';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/CardBlock',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'card_block',
        items: [],
        primary: {
          anchor: [
            {
              type: 'paragraph',
              text:
                'Enim cillum reprehenderit aliqua labore dolore est magna amet sunt. Commodo officia tempor aliqua consequat laborum anim aliquip fugiat incididunt ipsum officia reprehenderit consectetur elit dolore. Ullamco deserunt ullamco voluptate adipisicing et.',
              spans: [],
            },
          ],
          title: [{ type: 'heading1', text: 'Empower e-business methodologies', spans: [] }],
          description: [
            {
              type: 'paragraph',
              text: 'Dolor nostrud laborum duis occaecat duis elit.',
              spans: [],
            },
          ],
          image: {
            dimensions: { width: 900, height: 500 },
            alt: 'Placeholder image',
            copyright: null,
            url:
              'https://images.unsplash.com/photo-1498050108023-c5249f4df085?w=900&h=500&fit=crop',
          },
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
