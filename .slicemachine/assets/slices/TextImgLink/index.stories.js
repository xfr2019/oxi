import MyComponent from '../../../../slices/TextImgLink';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/TextImgLink',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'text_img_link',
        items: [
          {
            text: [
              {
                type: 'paragraph',
                text:
                  'Incididunt duis magna culpa irure sit nostrud nulla ex non non voluptate eiusmod sit eu. Sint amet laboris id proident cillum id velit veniam do dolore minim sit amet. Elit ad tempor consectetur irure quis tempor veniam veniam.',
                spans: [],
              },
            ],
            bigImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1587614295999-6c1c13675117?w=900&h=500&fit=crop',
            },
            linkLabel: [
              {
                type: 'paragraph',
                text:
                  'Mollit id magna cupidatat minim esse nostrud. Laboris amet consectetur ipsum velit esse ad consectetur magna enim.',
                spans: [],
              },
            ],
            linkImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1601933973783-43cf8a7d4c5f?w=900&h=500&fit=crop',
            },
            linkLabel2: [
              {
                type: 'paragraph',
                text:
                  'Laboris et cupidatat aute velit pariatur nostrud magna in eu eiusmod laborum incididunt. Aliqua aliquip incididunt occaecat enim velit. Minim excepteur nulla magna esse reprehenderit esse.',
                spans: [],
              },
            ],
            linkImg2: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1498050108023-c5249f4df085?w=900&h=500&fit=crop',
            },
          },
          {
            text: [
              {
                type: 'paragraph',
                text:
                  'Esse dolor tempor occaecat exercitation veniam enim velit irure proident tempor.',
                spans: [],
              },
            ],
            bigImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1576662712957-9c79ae1280f8?w=900&h=500&fit=crop',
            },
            linkLabel: [
              {
                type: 'paragraph',
                text: 'Nulla sit fugiat laboris laboris irure laborum sint.',
                spans: [],
              },
            ],
            linkImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1494173853739-c21f58b16055?w=900&h=500&fit=crop',
            },
            linkLabel2: [
              {
                type: 'paragraph',
                text:
                  'Ipsum aliqua id esse duis consectetur pariatur. Labore eiusmod culpa irure labore et velit ullamco irure enim cillum.',
                spans: [],
              },
            ],
            linkImg2: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1589321599763-d66926c29613?w=900&h=500&fit=crop',
            },
          },
          {
            text: [
              {
                type: 'paragraph',
                text:
                  'Id in irure et ullamco culpa laborum adipisicing elit non culpa officia dolore sit enim velit.',
                spans: [],
              },
            ],
            bigImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1587905069134-008460d7a636?w=900&h=500&fit=crop',
            },
            linkLabel: [
              {
                type: 'paragraph',
                text:
                  'Fugiat proident est elit eiusmod occaecat ut veniam incididunt cillum reprehenderit enim esse.',
                spans: [],
              },
            ],
            linkImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1547394765-185e1e68f34e?w=900&h=500&fit=crop',
            },
            linkLabel2: [
              {
                type: 'paragraph',
                text:
                  'Ipsum id tempor duis labore qui sint commodo id irure nisi pariatur culpa officia tempor.',
                spans: [],
              },
            ],
            linkImg2: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1579931794097-0ad001e51edb?w=900&h=500&fit=crop',
            },
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text:
                'Proident esse dolor veniam magna non nostrud voluptate ad id sint laboris velit incididunt sit eiusmod. Culpa nisi dolor proident do.',
              spans: [],
            },
          ],
          blockAnchor: [
            { type: 'paragraph', text: 'Occaecat pariatur irure non non eu.', spans: [] },
          ],
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
