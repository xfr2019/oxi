import MyComponent from '../../../../slices/PercentButtonImg';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/PercentButtonImg',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'percent_button_img',
        items: [],
        primary: {
          bigNumPercent: [
            {
              type: 'paragraph',
              text:
                'Ea magna laborum eu esse Lorem amet labore ad consequat eu. Nisi et non consectetur laborum dolor irure voluptate ut id exercitation esse adipisicing commodo.',
              spans: [],
            },
          ],
          bigText: [
            { type: 'paragraph', text: 'Officia adipisicing fugiat occaecat eu esse.', spans: [] },
          ],
          normalText: [
            {
              type: 'paragraph',
              text:
                'Commodo occaecat aute qui. Ut minim qui Lorem proident nisi irure exercitation ex.',
              spans: [],
            },
          ],
          buttonLabel: [
            {
              type: 'paragraph',
              text:
                'Do fugiat enim ex veniam proident excepteur dolor mollit commodo duis et laborum sunt. Labore veniam dolore adipisicing officia adipisicing voluptate proident culpa sit adipisicing non amet officia nulla.',
              spans: [],
            },
          ],
          buttonLink: [
            {
              type: 'paragraph',
              text:
                'Nostrud ex nulla nostrud tempor magna aliqua magna tempor in tempor est aute irure irure enim. Sunt irure occaecat fugiat enim enim proident velit. Veniam proident sunt qui officia eiusmod aliquip laboris magna amet.',
              spans: [],
            },
          ],
          img: {
            dimensions: { width: 900, height: 500 },
            alt: 'Placeholder image',
            copyright: null,
            url:
              'https://images.unsplash.com/photo-1596195689404-24d8a8d1c6ea?w=900&h=500&fit=crop',
          },
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
