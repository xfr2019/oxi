import MyComponent from '../../../../slices/MainScreen';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/MainScreen',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'main_screen',
        items: [
          {
            anchorLink: [
              {
                type: 'paragraph',
                text:
                  'Amet proident anim esse quis aute voluptate commodo ea pariatur amet Lorem elit culpa. Fugiat reprehenderit in elit velit proident veniam dolor incididunt sit aliquip aute esse mollit aliqua.',
                spans: [],
              },
            ],
            anchorLinkLabel: [
              {
                type: 'paragraph',
                text:
                  'Deserunt officia proident anim cupidatat. Elit deserunt velit ea incididunt laborum minim et officia dolore.',
                spans: [],
              },
            ],
          },
          {
            anchorLink: [
              { type: 'paragraph', text: 'Ex quis reprehenderit et et incididunt et.', spans: [] },
            ],
            anchorLinkLabel: [
              {
                type: 'paragraph',
                text: 'Aliqua fugiat enim enim commodo esse nostrud cillum ad aliqua elit.',
                spans: [],
              },
            ],
          },
          {
            anchorLink: [
              {
                type: 'paragraph',
                text:
                  'Eiusmod pariatur sit consectetur aute tempor proident cillum id sint pariatur incididunt dolore. Nostrud et dolor eu aliquip consequat ut cupidatat veniam non velit.',
                spans: [],
              },
            ],
            anchorLinkLabel: [
              { type: 'paragraph', text: 'Fugiat sunt id adipisicing.', spans: [] },
            ],
          },
        ],
        primary: {
          image: {
            dimensions: { width: 900, height: 500 },
            alt: 'Placeholder image',
            copyright: null,
            url:
              'https://images.unsplash.com/photo-1596195689404-24d8a8d1c6ea?w=900&h=500&fit=crop',
          },
          title: [{ type: 'heading1', text: 'Empower holistic e-services', spans: [] }],
          description: [
            {
              type: 'paragraph',
              text:
                'Laborum nisi reprehenderit ex commodo pariatur voluptate deserunt incididunt laboris cupidatat laboris minim laborum eu commodo.',
              spans: [],
            },
          ],
          subdescription: [
            {
              type: 'paragraph',
              text:
                'Enim ipsum id fugiat veniam proident adipisicing. Cupidatat adipisicing ex et.',
              spans: [],
            },
          ],
          buttonLabel: [{ type: 'paragraph', text: 'Magna nisi ex ad amet.', spans: [] }],
          buttonLink: [
            {
              type: 'paragraph',
              text:
                'Eiusmod velit aliqua nulla ullamco proident consequat voluptate eiusmod qui dolor velit est eu. Occaecat et duis et exercitation esse cillum sit laboris culpa amet elit laborum nisi. Ea exercitation tempor nulla nisi laborum reprehenderit.',
              spans: [],
            },
          ],
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
