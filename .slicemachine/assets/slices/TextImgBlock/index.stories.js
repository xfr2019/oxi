import MyComponent from '../../../../slices/TextImgBlock';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/TextImgBlock',
};

export const _RightImg = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'right_img',
        name: 'right_img',
        slice_type: 'text_img_block',
        items: [
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1471897488648-5eae4ac6686b?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text:
                  'Cillum mollit veniam adipisicing aliqua magna duis duis id consectetur anim aliquip adipisicing. Dolore dolor quis ex.',
                spans: [],
              },
            ],
            text: [
              {
                type: 'paragraph',
                text:
                  'Dolore dolore labore nostrud pariatur elit irure do consequat ad. Exercitation veniam do minim veniam ad Lorem Lorem minim.',
                spans: [],
              },
            ],
          },
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1571126770897-2d612d1f7b89?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text:
                  'Exercitation ipsum duis mollit laboris aute ad cupidatat cupidatat mollit minim cupidatat excepteur. Consequat anim excepteur cillum ut dolor cillum cillum esse officia dolor ea occaecat.',
                spans: [],
              },
            ],
            text: [
              {
                type: 'paragraph',
                text:
                  'Reprehenderit occaecat elit non laborum in non incididunt ea exercitation consectetur qui.',
                spans: [],
              },
            ],
          },
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1544731612-de7f96afe55f?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text:
                  'Labore commodo veniam voluptate mollit ut aliquip labore exercitation. Voluptate exercitation eiusmod veniam eu nostrud.',
                spans: [],
              },
            ],
            text: [
              { type: 'paragraph', text: 'In dolore irure anim in anim velit aliquip.', spans: [] },
            ],
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text:
                'Officia ea Lorem reprehenderit eu Lorem duis in ad do anim. Deserunt officia anim laborum labore consequat do consectetur duis.',
              spans: [],
            },
          ],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'Enim aliqua aliqua nulla elit dolor exercitation sint incididunt aute cillum pariatur cillum. Cupidatat aliquip occaecat deserunt eu.',
              spans: [],
            },
          ],
        },
        id: '_RightImg',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_RightImg.storyName = 'right_img';

export const _TopImg = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'top_img',
        name: 'top_img',
        slice_type: 'text_img_block',
        items: [
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1486312338219-ce68d2c6f44d?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text: 'Reprehenderit consequat tempor enim laboris tempor ut ut proident.',
                spans: [],
              },
            ],
            text: [
              {
                type: 'paragraph',
                text:
                  'Ea deserunt occaecat labore exercitation non. Cillum sint enim tempor reprehenderit fugiat irure sint elit consequat duis occaecat consectetur. Adipisicing ea labore proident duis velit amet nisi.',
                spans: [],
              },
            ],
          },
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1545239351-1141bd82e8a6?w=900&h=500&fit=crop',
            },
            title: [
              { type: 'paragraph', text: 'Ipsum mollit quis culpa magna commodo.', spans: [] },
            ],
            text: [
              {
                type: 'paragraph',
                text:
                  'Fugiat sunt incididunt sit elit non ullamco. Laboris dolore qui elit ea. Ullamco minim eu magna non.',
                spans: [],
              },
            ],
          },
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1471897488648-5eae4ac6686b?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text:
                  'Nisi sunt cillum laborum commodo nisi eiusmod minim consequat minim enim mollit.',
                spans: [],
              },
            ],
            text: [
              {
                type: 'paragraph',
                text:
                  'Exercitation Lorem sit laboris cupidatat ullamco deserunt consectetur culpa. Ipsum tempor dolore mollit excepteur sunt duis id ex anim occaecat et. Eu culpa est nulla magna reprehenderit do quis irure sit ipsum aliqua in.',
                spans: [],
              },
            ],
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text:
                'Qui velit irure reprehenderit veniam in id eiusmod officia veniam Lorem. Duis nostrud elit ipsum sint enim culpa culpa proident occaecat laboris consectetur minim est.',
              spans: [],
            },
          ],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'Proident elit reprehenderit anim veniam. Eu est tempor deserunt nostrud ullamco excepteur. Ea sint aute occaecat sunt consequat commodo sunt sint deserunt.',
              spans: [],
            },
          ],
        },
        id: '_TopImg',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_TopImg.storyName = 'top_img';

export const _BigNum = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'big_num',
        name: 'big_num',
        slice_type: 'text_img_block',
        items: [
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1544731612-de7f96afe55f?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text:
                  'Magna nisi id ex dolore Lorem velit tempor nisi reprehenderit fugiat ex officia esse. Labore et occaecat deserunt excepteur. Ullamco nisi officia irure quis aliqua quis.',
                spans: [],
              },
            ],
            text: [{ type: 'paragraph', text: 'Aliquip aliqua eu id.', spans: [] }],
          },
          {
            img: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1499951360447-b19be8fe80f5?w=900&h=500&fit=crop',
            },
            title: [
              {
                type: 'paragraph',
                text:
                  'Culpa sunt irure cillum aute. Est est id tempor mollit qui culpa. Laborum mollit adipisicing esse ad in.',
                spans: [],
              },
            ],
            text: [
              {
                type: 'paragraph',
                text:
                  'Veniam cillum cillum elit et voluptate magna mollit proident commodo esse. Excepteur id tempor eiusmod nulla sint et nostrud nostrud laborum.',
                spans: [],
              },
            ],
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text:
                'Minim qui nostrud ad elit eu do id dolor nisi adipisicing. Nostrud nulla ea eu ullamco tempor.',
              spans: [],
            },
          ],
          blockAnchor: [{ type: 'paragraph', text: 'Tempor sunt aliqua culpa.', spans: [] }],
        },
        id: '_BigNum',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_BigNum.storyName = 'big_num';
