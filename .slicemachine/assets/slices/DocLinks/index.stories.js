import MyComponent from '../../../../slices/DocLinks';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/DocLinks',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'doc_links',
        items: [
          {
            link: [
              {
                type: 'paragraph',
                text: 'Sint ea fugiat nulla sit voluptate ipsum in consectetur.',
                spans: [],
              },
            ],
          },
          {
            link: [
              {
                type: 'paragraph',
                text:
                  'Qui dolore cupidatat ipsum dolore veniam do in est fugiat amet consequat laboris. Laborum id fugiat exercitation laborum est exercitation exercitation proident deserunt. Dolor veniam nisi esse non ea.',
                spans: [],
              },
            ],
          },
          { link: [{ type: 'paragraph', text: 'Eu deserunt occaecat labore id est.', spans: [] }] },
          {
            link: [
              {
                type: 'paragraph',
                text:
                  'Qui mollit veniam ex ex labore nulla fugiat tempor ipsum esse dolore dolor nostrud. Culpa adipisicing commodo dolore deserunt ipsum sunt cillum est labore fugiat. Aliqua ut nisi excepteur ad est fugiat adipisicing aute dolor laboris duis pariatur anim.',
                spans: [],
              },
            ],
          },
        ],
        primary: {},
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
