import MyComponent from '../../../../slices/FiveColumnsCards';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/FiveColumnsCards',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'five_columns_cards',
        items: [
          {
            image: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1560457079-9a6532ccb118?w=900&h=500&fit=crop',
            },
            cardTitle: [
              {
                type: 'paragraph',
                text:
                  'Laborum ea duis elit culpa cillum duis officia cillum in. Voluptate elit cupidatat ex nostrud adipisicing ex commodo elit.',
                spans: [],
              },
            ],
          },
          {
            image: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1547082299-de196ea013d6?w=900&h=500&fit=crop',
            },
            cardTitle: [
              {
                type: 'paragraph',
                text:
                  'Laborum non sunt eiusmod quis cillum sunt. Velit officia laboris do et in irure minim aute id adipisicing dolore eiusmod laboris incididunt proident. Ad laboris exercitation cillum deserunt mollit pariatur proident Lorem id veniam ipsum.',
                spans: [],
              },
            ],
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text:
                'Magna ipsum est anim elit culpa sunt. Aute commodo veniam id nostrud et consequat.',
              spans: [],
            },
          ],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'Laboris ex sunt ipsum sunt cupidatat fugiat proident amet consectetur incididunt.',
              spans: [],
            },
          ],
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
