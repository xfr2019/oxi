import MyComponent from '../../../../slices/AddInfoLinks';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/AddInfoLinks',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'add_info_links',
        items: [
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Tempor sunt veniam anim est cupidatat et labore consectetur non dolore commodo ullamco nostrud. Duis ipsum officia officia enim id velit commodo in aute officia ad. Officia aute dolore incididunt aute ullamco nulla sit excepteur irure.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Ut incididunt ex qui ea. Irure voluptate fugiat sunt Lorem consectetur quis officia.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text: 'Laboris pariatur qui consectetur id aliquip minim aute.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Ut eu in cillum et. Sunt ut sit nulla mollit duis consectetur dolor reprehenderit. Proident incididunt do dolor sunt cupidatat excepteur ex enim sunt et minim voluptate.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Ullamco cupidatat quis veniam consectetur velit sint magna ut voluptate occaecat. Elit consectetur ullamco minim ipsum id anim elit sunt consequat nisi labore sit.',
                spans: [],
              },
            ],
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text:
                'Dolor nostrud elit eu eu culpa quis consequat Lorem ullamco velit ex fugiat. Ipsum dolor occaecat nisi anim pariatur excepteur sunt elit tempor anim excepteur occaecat.',
              spans: [],
            },
          ],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'In exercitation dolor eiusmod officia dolore occaecat officia irure non nostrud veniam.',
              spans: [],
            },
          ],
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';

export const _WithGrayBlock = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'withGrayBlock',
        name: 'WithGrayBlock',
        slice_type: 'add_info_links',
        items: [
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Nisi amet eiusmod do non ipsum non aliquip est officia. Ullamco est adipisicing dolore ut eu proident elit fugiat id commodo culpa aliqua tempor. Sunt deserunt id nisi ullamco aliqua laborum do elit ut duis exercitation excepteur exercitation adipisicing voluptate.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Commodo nulla occaecat et eiusmod et adipisicing elit fugiat. Est quis duis eiusmod cillum. Aliquip sunt ex culpa sunt reprehenderit adipisicing ex.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text: 'Nulla ex irure duis eu proident nostrud aliquip sunt.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Dolor do laboris laboris nostrud fugiat enim est minim cupidatat fugiat reprehenderit tempor velit aute. Eiusmod sit eu amet consectetur dolor non amet non nostrud eiusmod quis aliqua voluptate aliqua.',
                spans: [],
              },
            ],
          },
          {
            linkTitle: [
              {
                type: 'paragraph',
                text:
                  'Qui consectetur nostrud do. Est cupidatat officia eu anim sint pariatur ullamco deserunt tempor labore ipsum.',
                spans: [],
              },
            ],
          },
        ],
        primary: {
          blockTitle: [
            {
              type: 'paragraph',
              text: 'Laboris adipisicing sint nulla duis consectetur do eu.',
              spans: [],
            },
          ],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'Qui excepteur aliqua quis. Sunt nostrud mollit fugiat deserunt consequat dolore consequat ut Lorem ex minim.',
              spans: [],
            },
          ],
          GrayBlockTitle: [
            {
              type: 'paragraph',
              text:
                'Eu est incididunt duis ex ut aliquip culpa. Adipisicing do pariatur ex. Commodo enim est irure qui elit dolor irure aliquip ut ea nostrud cupidatat ea reprehenderit.',
              spans: [],
            },
          ],
          GrayBlockLink1: [
            {
              type: 'paragraph',
              text: 'Eu incididunt veniam officia velit deserunt officia magna reprehenderit.',
              spans: [],
            },
          ],
          GrayBlockLink2: [
            {
              type: 'paragraph',
              text:
                'Nostrud sunt consectetur minim esse proident elit id irure sint mollit nisi adipisicing non. Ut cillum do sit dolore non exercitation adipisicing aliqua labore. Eiusmod eiusmod eu enim nulla officia sint non incididunt adipisicing eiusmod aliquip laborum ipsum.',
              spans: [],
            },
          ],
        },
        id: '_WithGrayBlock',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_WithGrayBlock.storyName = 'WithGrayBlock';
