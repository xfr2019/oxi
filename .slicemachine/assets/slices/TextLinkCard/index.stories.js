import MyComponent from '../../../../slices/TextLinkCard';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/TextLinkCard',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'text_link_card',
        items: [
          {
            cardText: [
              {
                type: 'paragraph',
                text: 'Occaecat laborum fugiat sint eu laborum irure pariatur.',
                spans: [],
              },
            ],
            cardLink: { link_type: 'Web', url: 'https://prismic.io' },
            cardLinkLabel: [
              {
                type: 'paragraph',
                text:
                  'Veniam aute ipsum aliquip. Excepteur amet enim ea ullamco laborum adipisicing.',
                spans: [],
              },
            ],
          },
          {
            cardText: [
              {
                type: 'paragraph',
                text:
                  'Laboris est dolor exercitation excepteur Lorem enim nostrud. Mollit ad in consectetur mollit ex amet. Nulla amet exercitation elit commodo laborum do pariatur nulla commodo consectetur nulla.',
                spans: [],
              },
            ],
            cardLink: { link_type: 'Web', url: 'http://google.com' },
            cardLinkLabel: [
              {
                type: 'paragraph',
                text:
                  'Excepteur mollit consequat pariatur dolor nulla labore consequat eiusmod non ipsum anim quis est. Veniam ea est anim enim ut.',
                spans: [],
              },
            ],
          },
          {
            cardText: [
              {
                type: 'paragraph',
                text: 'Commodo sint esse consequat eu adipisicing cillum aute.',
                spans: [],
              },
            ],
            cardLink: { link_type: 'Web', url: 'https://slicemachine.dev' },
            cardLinkLabel: [
              {
                type: 'paragraph',
                text:
                  'Cillum adipisicing officia labore nostrud eu id eu dolor consectetur occaecat labore deserunt dolor adipisicing commodo. Culpa anim veniam dolor non ad sint nisi deserunt do irure cillum non fugiat labore.',
                spans: [],
              },
            ],
          },
          {
            cardText: [
              {
                type: 'paragraph',
                text:
                  'Occaecat cillum mollit laboris ex consequat aliqua. Sint nisi ex est elit laborum sit qui laborum Lorem reprehenderit. Lorem duis veniam excepteur aliqua culpa aliqua.',
                spans: [],
              },
            ],
            cardLink: { link_type: 'Web', url: 'http://twitter.com' },
            cardLinkLabel: [
              {
                type: 'paragraph',
                text:
                  'Commodo consequat adipisicing nulla mollit veniam eiusmod culpa laborum. Sint qui cillum amet elit consequat sint.',
                spans: [],
              },
            ],
          },
          {
            cardText: [
              {
                type: 'paragraph',
                text:
                  'Deserunt minim nostrud minim Lorem dolor. Aliqua voluptate labore dolor nostrud nisi est reprehenderit sint dolor dolor incididunt sit aliqua ea consequat.',
                spans: [],
              },
            ],
            cardLink: { link_type: 'Web', url: 'https://slicemachine.dev' },
            cardLinkLabel: [
              {
                type: 'paragraph',
                text:
                  'Veniam qui tempor reprehenderit dolore id sunt non et fugiat ullamco occaecat aliqua excepteur.',
                spans: [],
              },
            ],
          },
        ],
        primary: {
          blockTitle: [
            { type: 'paragraph', text: 'Deserunt ullamco veniam sit veniam.', spans: [] },
          ],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'Minim exercitation irure fugiat culpa. Ullamco sunt dolore ipsum ullamco labore nisi irure eiusmod eu incididunt do nulla. Cupidatat velit id aliqua minim pariatur amet eu ex aliquip incididunt minim.',
              spans: [],
            },
          ],
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
