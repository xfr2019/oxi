import MyComponent from '../../../../slices/SliderCard';
import SliceZone from 'vue-slicezone';

export default {
  title: 'slices/SliderCard',
};

export const _DefaultSlice = () => ({
  components: {
    MyComponent,
    SliceZone,
  },
  methods: {
    resolve() {
      return MyComponent;
    },
  },
  data() {
    return {
      mock: {
        variation: 'default-slice',
        name: 'Default slice',
        slice_type: 'slider_card',
        items: [
          {
            slideLabel: [
              {
                type: 'paragraph',
                text:
                  'Nisi voluptate incididunt deserunt magna. Enim ipsum id non occaecat consequat et magna est excepteur ullamco velit anim.',
                spans: [],
              },
            ],
            slideImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1547082299-de196ea013d6?w=900&h=500&fit=crop',
            },
          },
          {
            slideLabel: [
              {
                type: 'paragraph',
                text:
                  'Cupidatat minim magna nulla. Irure velit labore nostrud magna consectetur ullamco esse esse id deserunt. Est consectetur adipisicing incididunt nulla aliquip culpa.',
                spans: [],
              },
            ],
            slideImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1593642532973-d31b6557fa68?w=900&h=500&fit=crop',
            },
          },
          {
            slideLabel: [
              {
                type: 'paragraph',
                text:
                  'Mollit dolor do ea consectetur fugiat ex nisi sit nostrud officia consectetur do. Magna reprehenderit ea ea consequat est ex in ut ea aliqua consectetur id. Aute nisi cupidatat excepteur culpa quis ut.',
                spans: [],
              },
            ],
            slideImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url: 'https://images.unsplash.com/photo-1560762484-813fc97650a0?w=900&h=500&fit=crop',
            },
          },
          {
            slideLabel: [
              {
                type: 'paragraph',
                text:
                  'Ut dolore aute cillum cupidatat cillum reprehenderit consequat reprehenderit quis nisi.',
                spans: [],
              },
            ],
            slideImg: {
              dimensions: { width: 900, height: 500 },
              alt: 'Placeholder image',
              copyright: null,
              url:
                'https://images.unsplash.com/photo-1587840171670-8b850147754e?w=900&h=500&fit=crop',
            },
          },
        ],
        primary: {
          title: [{ type: 'heading1', text: 'Incentivize cross-platform metrics', spans: [] }],
          blockAnchor: [
            {
              type: 'paragraph',
              text:
                'Nostrud aliquip voluptate non aliquip culpa incididunt laborum ullamco culpa enim. Deserunt irure eu dolor nulla dolor est incididunt. Anim occaecat est consectetur sit quis eu.',
              spans: [],
            },
          ],
        },
        id: '_DefaultSlice',
      },
    };
  },
  template: '<SliceZone :slices="[mock]" :resolver="resolve" />',
});
_DefaultSlice.storyName = 'Default slice';
