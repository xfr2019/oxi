FROM node:current-alpine

RUN apk add --no-cache python make g++

WORKDIR /opt/app

ENV PATH /opt/app/node_modules/.bin:$PATH

COPY . .

RUN yarn
RUN yarn run build

EXPOSE 8080

CMD ["yarn", "run", "start"]
