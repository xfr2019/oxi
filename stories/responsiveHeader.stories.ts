import ResponsiveHeader from '~/layouts/header/ResponsiveHeader.vue';

export default {
  component: ResponsiveHeader,
  title: 'Components/Headers',
};

export const Responsive = () => ({
  components: { ResponsiveHeader },
  template: '<ResponsiveHeader />',
});
