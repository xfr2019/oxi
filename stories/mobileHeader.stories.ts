import MobileHeader from '~/layouts/header/MobileHeader.vue';

export default {
  component: MobileHeader,
  title: 'Components/Headers',
};

export const Mobile = () => ({
  components: { MobileHeader },
  template: '<MobileHeader />',
});
