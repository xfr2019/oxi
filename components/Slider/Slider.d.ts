export interface ISlide {
  image: string;
  title: string;
  description?: string;
  button: {
    label?: string;
    link?: string;
  };
  thumb?: {
    description?: string;
    image?: string;
  };
}
