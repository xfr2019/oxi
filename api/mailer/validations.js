import { body, validationResult } from 'express-validator';
import { validateAsync } from '../utils/validate';

const mailerValidationRules = {
  finmon: [body('violationEssence').notEmpty().isLength({ max: 100 }).escape()],
};

export const validateMailerRequest = async (req, res, next) => {
  await body('form_type').trim().isIn(Object.keys(mailerValidationRules)).run(req);

  const validationErrors = validationResult(req);
  if (!validationErrors.isEmpty()) {
    return res.status(400).json({ errors: validationErrors.array() });
  }

  return validateAsync(mailerValidationRules[req.body.form_type])(req, res, next);
};
