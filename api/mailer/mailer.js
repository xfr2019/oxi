import express from 'express';
import nodemailer from 'nodemailer';

require('dotenv').config();
const app = express();
const routePath = '/api/v1';
app.use(express.json({ limit: '20mb' }));
app.use(express.urlencoded({ limit: '20mb' }));

app.post('/mailer', async (req, res) => {
  const formType = req.body.form_type;
  const formSubject = req.body.subject;

  const html = getEmailHtml(getEmailBody(formType, req.body));

  const emailParams = {
    from: '<site@oxi.com>',
    to: 'info@oxibank.ua',
    subject: formSubject,
    html,
  };

  try {
    await sendEmail(emailParams);
    res.status(200).json({ ok: true });
  } catch (err) {
    // eslint-disable-next-line no-console
    console.error(err);
    res.status(503).json({ ok: false });
  }
});

const getEmailBody = (formType, requestData) => {
  switch (formType) {
    case 'request': {
      const { name, number, email } = requestData;
      return `<p>Ім’я: ${name}</p>
      <p>Номер телефону: ${number}</p>
      <p>E-mail: ${email}</p>`;
    }
    case 'question': {
      const { name, email, question, option } = requestData;
      return `<p>Напрямок: ${option}</p>
      <p>Ім’я: ${name}</p>
      <p>E-mail: ${email}</p>
      <p>Опишіть зміст (причину) звернення: ${question}</p>`;
    }
    case 'compliance': {
      const {
        name,
        number,
        email,
        date,
        question,
        employeeName,
        status,
        incidentType,
      } = requestData;
      return `<p>Прізвище, ім’я та по-батькові заявника: ${name}</p>
      <p>Номер телефону: ${number}</p>
      <p>E-mail: ${email}</p>
      <p>Статус заявника: ${status}</p>
      <p>Назва підрозділу Банку, ПIБ та посада особи, в результаті дій якого стався інцидент (вчинені неналежні дії): ${employeeName}</p>
      <p>Дата інциденту: ${date}</p>
      <p>Вид інциденту: ${incidentType}</p>
      <p>Детальна інформація про обставини інциденту: ${question}</p>`;
    }
    case 'finmon': {
      const {
        name,
        email,
        violationEssence,
        dateOne,
        dateTwo,
        total,
        participants,
        transactionContent,
        circumstances,
        legalization,
        beneficiaries,
        thirdParties,
        addInfo,
      } = requestData;
      return `<p>Прізвище, ім’я та по-батькові заявника: ${name}</p>
      <p>E-mail: ${email}</p>
      <p>Суть порушення (опишіть події, діяльність, та/або фінансові операції): ${violationEssence}</p>
      <p>Дата/період проведення фінансових операцій *: ${(dateOne, dateTwo)}</p>
      <p>Сума і валюта: ${total}</p>
      <p>Учасники фінансових операцій та їх роль: ${participants}</p>
      <p>Зміст фінансових операцій (форма розрахунку, вид активу тощо): ${transactionContent}</p>
      <p>Обставини (засоби), за яких були виявлені фінансові операції: ${circumstances}</p>
      <p>Наявність ознак ВК/ФТ: ${legalization}</p>
      <p>Дані про фактичних вигодоодержувачів (за наявності): ${beneficiaries}</p>
      <p>Дані про третіх осіб, які могли бути залученими до зазначених операцій: ${thirdParties}</p>
      <p>Будь-яка інша інформація про факти порушення у сфері ПВК/ФТ: ${addInfo}</p>`;
    }
    case 'internet_acquiring': {
      const { company, phone, email, customer_name } = requestData;
      return `<p>Назва компанії або посилання на сайт: ${company}</p>
      <p>Номер телефону для зв’язку: ${phone}</p>
      <p>E-mail: ${email}</p>
      <p>Як до Вас звертатися?: ${customer_name}</p>`;
    }
  }
};

const sendEmail = async (params) => {
  try {
    const transporter = nodemailer.createTransport({
      service: 'gmail',
      auth: { user: 'info@oxibank.ua', pass: '!password' },
    });

    const result = await transporter.sendMail(params);

    return result;
  } catch (err) {
    throw new Error(err);
  }
};

const getEmailHtml = (body) => {
  return `<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
    <html xmlns="http://www.w3.org/1999/xhtml">
     <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
      <title></title>
    </head>
    <body>
      ${body}
    </body>
    </html>`;
};

export { app as handler, routePath as path };
