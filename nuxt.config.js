// import getStoriesPaths from 'slice-machine-ui/helpers/storybook';

export default {
  head: {
    htmlAttrs: {
      lang: 'uk',
    },
    meta: [
      {
        charset: 'utf-8',
      },
      {
        name: 'viewport',
        content: 'width=device-width, initial-scale=1, shrink-to-fit=no',
      },
    ],
    script: [
      {
        src: `https://maps.googleapis.com/maps/api/js?key=AIzaSyDsjrl4foBfXXA2MfqwTLT-5L3F5m9zZSQ&libraries=places`,
      },
    ],
    link: [
      {
        rel: 'icon',
        type: 'image/x-icon',
        href: '/favicon.ico',
      },
    ],
  },
  publicRuntimeConfig: {
    storageDomain: process.env.STORAGE_DOMAIN || 'https://assets-oxi.ftr-dev.net',
    gmapApiKey: process.env.GOOGLE_MAPS_API_KEY,
  },
  css: ['@/assets/scss/custom.scss', 'swiper/css/swiper.css'],
  plugins: [
    { src: '~/plugins/autocomplete.js' },
    { src: '~/plugins/vue-awesome-swiper.js', mode: 'client' },
    { src: '~/plugins/datepicker.js' },
  ],
  serverMiddleware: [
    {
      path: '/mailer',
      handler: '~/api/mailer/mailer.js',
    },
  ],
  components: true,
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/svg',
    '@nuxtjs/stylelint-module',
    [
      '@nuxtjs/google-fonts',
      {
        families: {
          Raleway: {
            wght: [200, 400, 700],
          },
          'Open Sans': {
            wght: [200, 300, 400, 700],
          },
        },
        display: 'swap',
        prefetch: true,
      },
    ],
    '@nuxtjs/prismic',
  ],
  prismic: {
    endpoint: 'https://oxibank-website.cdn.prismic.io/api/v2',
    linkResolver: '@/plugins/link-resolver',
    htmlSerializer: '@/plugins/html-serializer',
    preview: false,
  },
  modules: ['bootstrap-vue/nuxt'],
  ignore: ['**/*.stories.js', '**/*.stories.js'],
  bootstrapVue: {
    bootstrapCSS: false,
    bootstrapVueCSS: false,
  },
  build: {
    extractCSS: true,
    transpile: ['vue-slicezone'],
  },
  storybook: {
    port: 3003,
    parameters: {
      layout: 'fullscreen',
      backgrounds: {
        default: 'white',
        values: [
          {
            name: 'white',
            value: '#ffffff',
          },
          {
            name: 'gray',
            value: '#aaaaaa',
          },
        ],
      },
      viewport: {
        viewports: {
          responsive: {
            name: 'responsive',
          },
          desktop: {
            name: 'desktop',
            styles: {
              width: '1281px',
              height: '700px',
            },
          },
          iphone5: {
            name: 'iPhone 5',
            styles: {
              padding: 0,
              height: '568px',
              width: '320px',
            },
            type: 'mobile',
          },
          iphone6: {
            name: 'iPhone 6',
            styles: {
              height: '667px',
              width: '375px',
            },
            type: 'mobile',
          },
          iphone6p: {
            name: 'iPhone 6 Plus',
            styles: {
              height: '736px',
              width: '414px',
            },
            type: 'mobile',
          },
          iphone8p: {
            name: 'iPhone 8 Plus',
            styles: {
              height: '736px',
              width: '414px',
            },
            type: 'mobile',
          },
          iphonex: {
            name: 'iPhone X',
            styles: {
              height: '812px',
              width: '375px',
            },
            type: 'mobile',
          },
          iphonexr: {
            name: 'iPhone XR',
            styles: {
              height: '896px',
              width: '414px',
            },
            type: 'mobile',
          },
          iphonexsmax: {
            name: 'iPhone XS Max',
            styles: {
              height: '896px',
              width: '414px',
            },
            type: 'mobile',
          },
          iphonese2: {
            name: 'iPhone SE (2nd generation)',
            styles: {
              height: '667px',
              width: '375px',
            },
            type: 'mobile',
          },
          iphone12mini: {
            name: 'iPhone 12 mini',
            styles: {
              height: '812px',
              width: '375px',
            },
            type: 'mobile',
          },
          iphone12: {
            name: 'iPhone 12',
            styles: {
              height: '844px',
              width: '390px',
            },
            type: 'mobile',
          },
          iphone12promax: {
            name: 'iPhone 12 Pro Max',
            styles: {
              height: '926px',
              width: '428px',
            },
            type: 'mobile',
          },
          ipad: {
            name: 'iPad',
            styles: {
              height: '1024px',
              width: '768px',
            },
            type: 'tablet',
          },
          ipad10p: {
            name: 'iPad Pro 10.5-in',
            styles: {
              height: '1112px',
              width: '834px',
            },
            type: 'tablet',
          },
          ipad12p: {
            name: 'iPad Pro 12.9-in',
            styles: {
              height: '1366px',
              width: '1024px',
            },
            type: 'tablet',
          },
          galaxys5: {
            name: 'Galaxy S5',
            styles: {
              height: '640px',
              width: '360px',
            },
            type: 'mobile',
          },
          galaxys9: {
            name: 'Galaxy S9',
            styles: {
              height: '740px',
              width: '360px',
            },
            type: 'mobile',
          },
          nexus5x: {
            name: 'Nexus 5X',
            styles: {
              height: '660px',
              width: '412px',
            },
            type: 'mobile',
          },
          nexus6p: {
            name: 'Nexus 6P',
            styles: {
              height: '732px',
              width: '412px',
            },
            type: 'mobile',
          },
          pixel: {
            name: 'Pixel',
            styles: {
              height: '960px',
              width: '540px',
            },
            type: 'mobile',
          },
          pixelxl: {
            name: 'Pixel XL',
            styles: {
              height: '1280px',
              width: '720px',
            },
            type: 'mobile',
          },
        },
      },
    },
    stories: ['~/stories/**/*.stories.@(ts|js)'],
  },
  server: {
    host: '0.0.0.0',
    port: 8080,
  },
};
