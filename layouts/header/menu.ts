interface MenuNode {
  id?: string;
  label: string;
  path: string;
  children?: MenuNode[];
}

export interface Menu {
  business_menu: MenuNode[];
  private_menu: MenuNode[];
}

const aboutBankMenu: MenuNode = {
  id: 'about_bank',
  label: 'Про банк',
  path: '/#',
  children: [
    {
      label: 'Про банк',
      path: '/about-bank',
    },
    {
      label: 'Реалізація заставного майна',
      path: '/#',
    },
    {
      label: 'Контроль комплаенс ризиків',
      path: '/#',
    },
    {
      label: 'Співробітництво',
      path: '/#',
    },
    {
      label: 'Вакансії',
      path: '/#',
    },
    {
      id: 'partners',
      label: 'Партнери',
      path: '/#',
      children: [
        {
          label: 'Страхові компанії',
          path: '/#',
        },
        {
          label: 'Кредитні посередники',
          path: '/#',
        },
        {
          label: 'sportbank',
          path: '/#',
        },
        {
          label: 'Діджиталбанк',
          path: '/#',
        },
      ],
    },
  ],
};

const menu: Menu = {
  business_menu: [
    {
      id: 'credit',
      label: 'Кредити',
      path: '/business/loans',
      children: [
        {
          label: 'Овердрафт',
          path: '/business/loans/overdraft',
        },
        {
          label: 'Беззаставний овердрафт',
          path: '/business/loans/unsecured-overdraft',
        },
        {
          label: 'Кредит на придбання основних засобів',
          path: '/business/loans/fixed-assets-acquisition-loan',
        },
        {
          label: 'Кредит на поповнення обігових коштів',
          path: '/business/loans/working-capital-financing',
        },
        {
          label: 'Кредит під депозит',
          path: '/business/loans/loan-under-deposit',
        },
        {
          label: 'Всі кредити',
          path: '/business/loans',
        },
      ],
    },
    {
      id: 'deposit',
      label: 'Депозити',
      path: '/business/deposits',
      children: [
        {
          label: 'Депозитна лінія',
          path: '/business/deposits/deposit-facility',
        },
        {
          label: 'Бізнес',
          path: '/business/deposits/deposit-business',
        },
        {
          label: 'Бізнес +',
          path: '/business/deposits/deposit-business-plus',
        },
        {
          label: 'Зростаючий дохід',
          path: '/business/deposits/deposit-increasing-profit',
        },
        {
          label: 'Всі депозити',
          path: '/business/deposits',
        },
      ],
    },
    {
      id: 'settlement_operations',
      label: 'Розрахункові операції',
      path: '/#',
      children: [
        {
          label: 'РКО',
          path: '/business/cash-and-settlement-payments',
        },
        {
          label: 'Валютні операції',
          path: '/business/operations-in-foreign-currency',
        },
      ],
    },
    {
      id: 'bank_guarantees',
      label: 'Документарні операції',
      path: '/#',
      children: [
        {
          label: 'Банківські гарантії',
          path: '/business/bank-guarantees',
        },
      ],
    },
    {
      id: 'cards',
      label: 'Картки',
      path: '/business/cards',
      children: [
        {
          label: 'Корпоративна картка',
          path: '/business/cards/corporate-card',
        },
      ],
    },
    {
      id: 'internet-acquiring',
      label: 'Інтернет-еквайринг',
      path: '/business/internet-acquiring',
    },
    aboutBankMenu,
  ],
  private_menu: [
    {
      id: 'credit_private',
      label: 'Кредити',
      path: '/private/loans',
      children: [
        {
          label: 'Житло в кредит',
          path: '/private/loans/mortgage',
        },
        {
          label: 'Споживчий кредит під заставу нерухомості',
          path: '/private/loans/consumer-residential-mortgage-loan',
        },
        {
          label: 'Авто в кредит',
          path: '/private/loans/car-loan',
        },
        {
          label: 'Споживчий кредит під заставу авто',
          path: '/private/loans/consumer-car-secured-loan',
        },
        {
          label: 'Всі кредити',
          path: '/private/loans',
        },
      ],
    },
    {
      id: 'deposit_private',
      label: 'Депозити',
      path: '/private/deposits',
      children: [
        {
          label: 'Стандартний',
          path: '/private/deposits/standard',
        },
        {
          label: 'Стандартний +',
          path: '/private/deposits/standard-plus',
        },
        {
          label: 'Доступний +',
          path: '/private/deposits/affordable-plus',
        },
        {
          label: 'Всі депозити',
          path: '/private/deposits',
        },
      ],
    },
    {
      id: 'cards',
      label: 'Картки',
      path: '/private/cards',
      children: [
        {
          label: 'Приватна картка',
          path: '/private/cards/private-card',
        },
        {
          label: 'Зарплатна картка',
          path: '/private/cards/salary-card',
        },
        {
          label: 'Депозитна картка',
          path: '/private/cards/card-under-deposit',
        },
        {
          label: 'Картка Новий Старт',
          path: '/private/cards/new-start',
        },
        {
          label: 'Всі картки',
          path: '/private/cards',
        },
      ],
    },
    aboutBankMenu,
  ],
};
function responsiveMenuAdapter(menu: Menu) {
  return Object.keys(menu).reduce<Menu>((acc, key) => {
    acc[key] = menu[key].filter(({ id }) => id !== 'about_bank');
    return acc;
  }, {} as Menu);
}

export const responsiveMenu = responsiveMenuAdapter(menu);
export default menu;
