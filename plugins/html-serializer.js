import prismicDOM from 'prismic-dom';
const Elements = prismicDOM.RichText.Elements;

export default function (type, element) {
  // If the image is also a link to a Prismic Document, it will return a <router-link> component
  if (type === Elements.image) {
    if (element.linkTo) {
      if (element.linkTo.link_type === 'Document') {
        return `<img src="${element.url}" alt="${element.alt || ''}" copyright="${
          element.copyright || ''
        }">`;
      }
    }
  }

  // Return null to stick with the default behavior for everything else
  return null;
}
