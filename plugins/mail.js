import axios from '@/plugins/axios';

export const sendEmail = (emailData) => {
  return axios.post('/api/v1/mailer', emailData);
};
