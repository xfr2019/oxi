import Vue from 'vue';
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker';

import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css';

const datepickerOptions = {
  daysShort: ['Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб', 'Нд'],
  monthNames: [
    'Січень',
    'Лютий',
    'Березень',
    'Квітень',
    'Травень',
    'Червень',
    'Липень',
    'Серпень',
    'Вересень',
    'Жовтень',
    'Листопад',
    'Грудень',
  ],
  colors: {
    selected: '#007FF2',
    inRange: '#ECEDEF',
    selectedText: '#fff',
    text: '#00183C',
    inRangeBorder: '#e4e7e7',
    disabled: '#fff',
    hoveredInRange: '#ECEDEF',
  },
  texts: {
    apply: 'Обрати',
    cancel: 'Відмінити',
  },
};

Vue.use(AirbnbStyleDatepicker, datepickerOptions);
